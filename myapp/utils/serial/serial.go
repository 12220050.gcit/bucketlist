package serial

import (
	"github.com/rs/xid"
)

func GenerateSerialNumber() string {
	serial := xid.New()
	return serial.String()
}
