package routes

import (
	"myapp/myapp/controller"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)


func InitializedRouter() {
	// resgitering route and mapping the handler function
	router := mux.NewRouter()
	router.HandleFunc("/home", controller.HomeHandler)
	router.HandleFunc("/urlParameter/{myname}", controller.ParameterHandler)

	//admin
	router.HandleFunc("/signup", controller.Signup).Methods("POST")
	router.HandleFunc("/signup/{email}", controller.UpdateUser).Methods("PUT")
	router.HandleFunc("/signup/{email}", controller.DeleteUsers).Methods("DELETE")
	router.HandleFunc("/signup/{email}", controller.GetUser).Methods("GET")
	router.HandleFunc("/signups", controller.GetAllUsers)
	router.HandleFunc("/login", controller.Login).Methods("POST")
	// router.HandleFunc("/signup", controller.DeleteAccount).Methods("DELETE")

	router.HandleFunc("/bucket", controller.CreateBucket).Methods("POST")
	// router.HandleFunc("/bucket/{serialNo}", controller.UpdateUser).Methods("PUT")
	// router.HandleFunc("/bucket/{serialNo}", controller.DeleteUsers).Methods("DELETE")
	// router.HandleFunc("/bucket/{serialNo}", controller.GetUser).Methods("GET")
	router.HandleFunc("/buckets", controller.GetList)
	// router.HandleFunc("/login", controller.Login).Methods("POST")

	// to serve static files
	fhandler := http.FileServer(http.Dir("./myapp/view"))
	router.PathPrefix("/").Handler(fhandler)
		// starting server
	err := http.ListenAndServe(":8081", router)
	if err != nil {
		os.Exit(1)
	}
	
}