package model

import "myapp/myapp/datastore/postgres"



type Bucket struct {
	SerialNo string
	Name    string
	Description  string
	Categories string
	Location string
	Status string
}

const queryInsertBucket = " INSERT INTO bucket(serialNo,Name,Description, Categories, Location, Status) VALUES($1,$2,$3, $4, $5,$6);"
const queryGetBucket = "SELECT serialNo,Name,Description, Categories, Location, Status FROM bucket WHERE serialNo=$1;"

// const (
// 	queryGetUser    = "Select name,email,password from admin where email=$1"
// 	queryDeleteUser = "DELETE from admin where email= $1;"
// 	queryUpdateUser = "UPDATE admin SET name=$1, email=$2,  password=$3 where email=$4;"
// )


func (b *Bucket) Create() error {
	_, err := postgres.Db.Exec(queryInsertBucket,  b.SerialNo, b.Name, b.Description, b.Categories, b.Location, b.Status)
	return err
}
func (b *Bucket) Get() error{
	return postgres.Db.QueryRow(queryGetBucket, b.SerialNo, b.Name, b.Description, b.Categories, b.Location, b.Status).Scan(&b.SerialNo)
}



func GetAllList()([]Bucket, error){
	rows, getErr := postgres.Db.Query("SELECT serialNo,Name,Description, Categories, Location, Status FROM bucket")
	if getErr != nil{
		return nil, getErr
	}
	//create a slice of type Course
	enrolls:= []Bucket{}

	for rows.Next(){
		var e Bucket
		dbErr := rows.Scan(&e.SerialNo, &e.Name,&e.Description, &e.Categories, &e.Location, &e.Status)
		if dbErr != nil{
			return nil, dbErr
		}
		enrolls = append(enrolls, e)

	}
	rows.Close()
	return enrolls, nil
}

// func (e *Enroll)Delete() error{
// 	if _, err:= postgres.Db.Exec( queryDeleteEnroll, e.StdId, e.CourseID); err != nil{
// 		return err
// 	}
// 	return nil
// }