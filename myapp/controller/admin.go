// package controller

// import (
// 	"encoding/json"
// 	"myapp/model"
// 	"myapp/utils/httpResp"
// 	"net/http"
// )

// func Signup(w http.ResponseWriter, r *http.Request){
// 	var admin model.Admin
// 	// fmt.Println(stud)
// 	decoder := json.NewDecoder(r.Body)
// 	err := decoder.Decode(&admin)
// 	if err != nil{
// 		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid Json Data")
// 		return
// 	}

// 	saveErr := admin.Create()
// 	// fmt.Println(saveErr)
// 	if saveErr != nil{
// 		httpResp.RespondWithError(w, http.StatusBadRequest, saveErr.Error())
// 		return
// 	}
// 	// w.Write([]byte ("student data added"))
// 	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"message": "admin added"})
// }

package controller

import (
	"encoding/json"
	"myapp/myapp/model"
	"myapp/myapp/utils/httpResp"

	"net/http"

	"github.com/gorilla/mux"
)

func Signup( w http.ResponseWriter, r *http.Request){
	var admin model.Admin
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&admin);err != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()
	saveErr:= admin.Create()
	if saveErr != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, saveErr.Error())
		return
	}
	// //no error
	httpResp.RespondWithJson(w, http.StatusCreated, map[string]string{"status": "admin added"})
}
func Login(w http.ResponseWriter, r *http.Request) {
	var admin model.Admin
	err := json.NewDecoder(r.Body).Decode(&admin)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()
	
	getErr := admin.Get()
	if getErr != nil {
	httpResp.RespondWithError(w, http.StatusUnauthorized, getErr.Error())
	return
	}
	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"message":"success"})
}

func GetUser(w http.ResponseWriter, r *http.Request) {
	email := mux.Vars(r)["email"]
	user, getErr := model.GetUserEmail(email)
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, user)
}

func GetAllUsers(w http.ResponseWriter, r *http.Request) {
	users, getErr := model.GetAll()

	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusCreated, users)
}

func DeleteUsers(w http.ResponseWriter, r *http.Request) {
	email := mux.Vars(r)["email"]

	err := model.DeleteUsers(email)

	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"status": "Deleted"})
}

func UpdateUser(w http.ResponseWriter, r *http.Request) {
	email := mux.Vars(r)["email"]
	var user model.Admin
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&user)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid json body")
		return
	}
	defer r.Body.Close()

	result, err := model.UpdateUser(user, email)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, result)
}
