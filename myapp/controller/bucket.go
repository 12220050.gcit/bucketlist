package controller

import (
	"encoding/json"
	"myapp/myapp/model"
	"myapp/myapp/utils/httpResp"
	"myapp/myapp/utils/serial"
	"net/http"
	"strings"
)

// func CreateBucket(w http.ResponseWriter, r *http.Request){
// 	// if !VerifyCookie(w,r){
// 	// 	return
// 	// }
// 	var buck model.Bucket
// 	// fmt.Println(stud)
// 	decoder := json.NewDecoder(r.Body)
// 	buck.SerialNo = serial.GenerateSerialNumber()
// 	err := decoder.Decode(&buck)
// 	if err != nil{
// 		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid Json Data")
// 		return
// 	}

// 	dbErr := buck.Create()
// 	// fmt.Println(dbErr)
// 	if dbErr != nil{
// 		httpResp.RespondWithError(w, http.StatusBadRequest, dbErr.Error())
// 		return
// 	}
// 	// w.Write([]byte ("student data added"))
// 	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"message": "bucket data added"})

// }

func CreateBucket(w http.ResponseWriter, r *http.Request) {
	var buck model.Bucket
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&buck); err != nil {
			httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
			return
		}
		buck.SerialNo = serial.GenerateSerialNumber()
		defer r.Body.Close()
		saveErr := buck.Create()
		if saveErr != nil {
			if strings.Contains(saveErr.Error(), "duplicate key") {
				httpResp.RespondWithError(w, http.StatusForbidden, "Duplicate keys")
			return
		} else {
			httpResp.RespondWithError(w, http.StatusInternalServerError, saveErr.Error())
		}
	}
	// no error
	httpResp.RespondWithJson(w, http.StatusCreated, map[string]string{"status": "list added"})
}

// func GetStud(w http.ResponseWriter, r *http.Request){
// 	// if !VerifyCookie(w,r){
// 	// 	return 
// 	// }
// 	//get url parameter
// 	sn := mux.Vars(r)["serialNo"]
// 	stud := model.Bucket{SerialNo: sn}
// 	// fmt.Println(stud)
// 	getErr := stud.Read()
// 	if getErr != nil{
// 		switch getErr{
// 		case sql.ErrNoRows:
// 			httpResp.RespondWithError(w, http.StatusNotFound, "student not found")
// 		default:
// 			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
// 		}
	
// 	} else{
// 		httpResp.RespondWithJson(w, http.StatusOK, stud)
// 	}

// }

// func UpdateStud(w http.ResponseWriter, r *http.Request){
// 	if !VerifyCookie(w,r){
// 		return 
// 	}
// 	old_sid := mux.Vars(r)["sid"]
// 	old_stdId:= getUserId(old_sid)
// 	//student instance
// 	var stud model.Student
// 	//json object
// 	decoder := json.NewDecoder(r.Body)
	
// 	if err:= decoder.Decode(&stud); err != nil{
// 		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
// 		return
// 	}
// 	getErr := stud.Update(old_stdId)
// 	if getErr != nil{
// 		switch getErr{
// 		case sql.ErrNoRows:
// 			httpResp.RespondWithError(w, http.StatusNotFound, "student not found")
// 		default:
// 			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
// 		}
	
// 	} else{
// 		httpResp.RespondWithJson(w, http.StatusOK, stud)
// 	}
// }

// func DeleteStud(w http.ResponseWriter, r *http.Request){
// 	// if !VerifyCookie(w,r){
// 	// 	return 
// 	// }
// 	sid := mux.Vars(r)["sid"]
// 	stdId:= getUserId(sid)
// 	s := model.Student{StdId: stdId}
// 	//student instance
// 	if err:= s.Delete(); err != nil{
// 		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
// 		return
// 	}
// 	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"status":"deleted"})
// }


func GetList (w http.ResponseWriter, r *http.Request){
	enrolls, getErr := model.GetAllList()
	if getErr != nil{
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, enrolls)

}