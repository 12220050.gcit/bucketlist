///new code
var reader;

function readURL(input) {
  if (input.files && input.files[0]) {

    reader = new FileReader();

    reader.onload = function(e) {
      $('.image-upload-wrap').hide();

      $('.file-upload-image').attr('src', e.target.result);
      $('.file-upload-content').show();

      $('.image-title').html(input.files[0].name);

      // createNewDiv(reader.result);
    };

    reader.readAsDataURL(input.files[0]);

  } else {
    removeUpload();
  }
}

function removeUpload() {
  $('.file-upload-input').replaceWith($('.file-upload-input').clone());
  $('.file-upload-content').hide();
  $('.image-upload-wrap').show();
}

$('.image-upload-wrap').bind('dragover', function () {
  $('.image-upload-wrap').addClass('image-dropping');
});

$('.image-upload-wrap').bind('dragleave', function () {
  $('.image-upload-wrap').removeClass('image-dropping');
});

function createNewDiv(imageUrl) {
  // Get the user input values
  var text = document.getElementById("text").value;

  // Create a new div element
  var newDiv = document.createElement("div");
  var newDiv2 = document.createElement("div");

// // Set the ID of the new div element
//   newDiv.setAttribute("id", "newDiv");

  // Set some attributes for the new div
  newDiv.className = "new-card";
  newDiv2.className = "new-card-labels";

  // Create a new image element
  var image = document.createElement("img");

  // Set the source attribute for the image
  image.src = imageUrl;

  // Create a new text element
  var textNode = document.createTextNode(text);
  textNode.className = "new-name";


  // Create a new <i> element
  var iconNode1 = document.createElement("i");
  iconNode1.className = "edit-btn";


  // Create a new <i> element
  var iconNode2 = document.createElement("i");
  iconNode2.className = "delete-btn";


  // Get the existing icon node
var edit = document.getElementById("edit");
var del = document.getElementById("delete")

// Add the Font Awesome user icon class to the existing class list
iconNode1.appendChild(edit)
iconNode2.appendChild(del)


  // Append the image and text elements to the new div
  newDiv.appendChild(image);
  newDiv2.appendChild(textNode);
  newDiv2.appendChild(iconNode1);
  newDiv2.appendChild(iconNode2);


  // Append the new div to the container element
  var container = document.getElementById("container");
  container.appendChild(newDiv);
  container.appendChild(newDiv2);

  removeUpload();
  reset()
}

function reset() {
  var text = document.getElementById("text").value = ""

}

var image =  document.getElementsByTagName("img");
var name = document.getElementsByClassName("card-label")

function delete_() {
  image.remove()
  // name.remove()
}